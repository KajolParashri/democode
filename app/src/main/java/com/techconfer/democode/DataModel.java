package com.techconfer.democode;

public class DataModel {

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    String image;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    String status;

    public DataModel(String Image, String Status) {
        this.image = Image;
        this.status = Status;

    }
}
