package com.techconfer.democode;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    int scorePoint = 0;
    TextView score;
    ArrayList<DataModel> dataModelslist;
    int random_number = 0;
    Timer timer, timer2;
    ItemAdapter itemAdapter;
    boolean isClick;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dataModelslist = new ArrayList<>();
        score = findViewById(R.id.score);
        recyclerView = findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        recyclerView.setHasFixedSize(true);

        getcolor();

        itemAdapter = new ItemAdapter(getApplicationContext(), dataModelslist, new onBoxClick() {
            @Override
            public void onClickonBox(int pos) {
                isClick = true;

                if (dataModelslist.get(pos).getStatus().equals("Active")) {
                    scorePoint++;
                    score.setText("Score " + scorePoint);
                    Toast.makeText(MainActivity.this, "Bingo", Toast.LENGTH_SHORT).show();
                } else {
                    timer.cancel();
                    showpopup(scorePoint);
                }
            }
        });
        recyclerView.setAdapter(itemAdapter);


        // And From your main() method or any other method
        timer = new Timer();
        timer.schedule(new ChangeToGrey(), 0, 1000);
    }


    public int getRandomNum() {
        Random r = new Random();
        int i1 = r.nextInt(4 - 1) + 1;
        return i1;
    }


    public void getcolor() {
        DataModel dataModel1 = new DataModel("#EB7C31", "Inactive");
        dataModelslist.add(dataModel1);

        DataModel dataModel2 = new DataModel("#5B9AD4", "Inactive");
        dataModelslist.add(dataModel2);

        DataModel dataModel3 = new DataModel("#FDBF00", "Inactive");
        dataModelslist.add(dataModel3);

        DataModel dataModel4 = new DataModel("#6FAC47", "Inactive");
        dataModelslist.add(dataModel4);
    }

    class ChangeToGrey extends TimerTask {
        public void run() {

            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    if(!isClick){
                        timer.cancel();
                        showpopup(scorePoint);
                    }

                    if (dataModelslist.get(random_number).getStatus().equals("Active")) {
                        dataModelslist.get(random_number).setStatus("Inactive");
                        itemAdapter.notifyItemChanged(random_number);
                        isClick = false;
                    } else {
                        random_number = getRandomNum();
                        dataModelslist.get(random_number).setStatus("Active");
                        itemAdapter.notifyItemChanged(random_number);
                        isClick = false;
                    }
                }
            });

        }
    }


    public void showpopup(final int scoree) {
        final Dialog dialog = new Dialog(this);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(R.layout.gameover_popup);


        TextView textView = dialog.findViewById(R.id.text1);
        TextView textView2 = dialog.findViewById(R.id.text2);
        textView2.setText("Your score is " + scoree);

        Button restart = dialog.findViewById(R.id.restart);
        restart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scorePoint = 0;
                score.setText("Score: 0");
                dialog.dismiss();
                // And From your main() method or any other method
                timer = new Timer();
                timer.schedule(new ChangeToGrey(), 1000, 1000);


            }
        });

        dialog.show();
    }



}

