package com.techconfer.democode;

import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.MyViewHolder> {

    List<DataModel> dataModels;
    Context context;
    DataModel dataModel;
    onBoxClick onBoxClick;
    MyViewHolder viewHolder;

    public ItemAdapter(Context context, ArrayList<DataModel> dataModelslist,onBoxClick onBoxClick) {
        this.context = context;
        this.dataModels = dataModelslist;
        this.onBoxClick=onBoxClick;
    }

    @NonNull
    @Override
    public ItemAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_color, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ItemAdapter.MyViewHolder holder, final int position) {

        dataModel = dataModels.get(position);
        viewHolder = holder;

        if (dataModel.getStatus().equals("Active")) {
            holder.image.setBackgroundColor(Color.parseColor("#666666"));
        } else {
            holder.image.setBackgroundColor(Color.parseColor(dataModel.getImage()));
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBoxClick.onClickonBox(position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return dataModels.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView image;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            image = itemView.findViewById(R.id.item);
        }

    }



}
